﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(appveyor_test.Startup))]
namespace appveyor_test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
