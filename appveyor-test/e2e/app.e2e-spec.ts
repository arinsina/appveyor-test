import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

describe('appveyor-test App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');

  });

  it('should display sometext when click on test button', () => {
    page.navigateTo();

    element(by.css("#button")).click();
    expect(element(by.css("#sometext")).getText()).toBe('sometext');

  });

});
