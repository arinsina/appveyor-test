# appveyor-test

# AppveyorTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## quirks visual studio
add this to csproj file to disable typescript compilation
<TypeScriptCompileBlocked>true</TypeScriptCompileBlocked>

add these to include files in package
<PropertyGroup>
    <CopyAllFilesToSingleFolderForPackageDependsOn>
      CustomCollectFiles;
      $(CopyAllFilesToSingleFolderForPackageDependsOn);
    </CopyAllFilesToSingleFolderForPackageDependsOn>

    <CopyAllFilesToSingleFolderForMsdeployDependsOn>
      CustomCollectFiles;
      $(CopyAllFilesToSingleFolderForMsdeployDependsOn);
    </CopyAllFilesToSingleFolderForMsdeployDependsOn>
  </PropertyGroup>

 <Target Name="CustomCollectFiles">
    <ItemGroup>
      <_CustomFiles Include="dist\**\*" />

      <FilesForPackagingFromProject  Include="%(_CustomFiles.Identity)">
        <DestinationRelativePath>dist\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
      </FilesForPackagingFromProject>
    </ItemGroup>
  </Target>



  to exclude file

<ItemGroup>
    <ExcludeFromPackageFiles Include="e2e\app.po.ts">
      <FromTarget>Project</FromTarget>
    </ExcludeFromPackageFiles>
  </ItemGroup>
  
  to exlude folder
  <ItemGroup>
    <ExcludeFromPackageFolders Include="src">
      <FromTarget>Project</FromTarget>
    </ExcludeFromPackageFolders>
  </ItemGroup>
